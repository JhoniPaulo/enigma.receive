﻿using Enigma.Receive.Domain.Models;

namespace Enigma.Receive.BLL
{  
    public class CommonBLL
    {
        private static readonly string _baseAddress = "http://localhost:3000/";
        public static async Task<ReturnStatusModel> Post(string json, string url)
        {
            using (var httpClient = new HttpClient { BaseAddress = new Uri(_baseAddress) })
            {
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("accept", "application/json");

                try
                {
                    using (var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json"))
                    {
                        using (var response = await httpClient.PostAsync($"{url}", content))
                        {
                            var message = await response.Content.ReadAsStringAsync();

                            return new ReturnStatusModel()
                            {
                                Message = message,
                                StatusCode = response.StatusCode
                            };
                        }
                    }
                }
                catch (Exception e)
                {
                    throw;
                }
            }
        }
    }
}
