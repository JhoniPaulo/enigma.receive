﻿using Enigma.Receive.Domain.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Enigma.Receive.BLL
{
    public class EnigmaPushBLL
    {
        public static bool SendMessage(string message)
        {
            var json = JsonConvert.SerializeObject(new { key = message });
            ReturnStatusModel res;
            try
            {
                res = BLL.CommonBLL.Post(json, "decrypt").Result;
            }
            catch (Exception)
            {
                return false;
            }

            if (res.StatusCode != HttpStatusCode.OK)
                return false;

            return true;
        }

        public static bool SendPhonesList(List<string> phones)
        {
            var json = JsonConvert.SerializeObject(phones);
            ReturnStatusModel res;
            try
            {
                res = BLL.CommonBLL.Post(json, "notification").Result;
            }
            catch (Exception)
            {
                return false;
            }

            if (res.StatusCode != HttpStatusCode.OK || res.StatusCode != HttpStatusCode.Created)
                return false;

            return true;
        }
    }
}
