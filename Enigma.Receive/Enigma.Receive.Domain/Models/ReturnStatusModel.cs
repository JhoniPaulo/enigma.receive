﻿

namespace Enigma.Receive.Domain.Models
{
    public class ReturnStatusModel
    {
        public System.Net.HttpStatusCode StatusCode { get; set; }
        public string Message { get; set; }
    }
}
