var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.MapPost("/decrypt", async (string message) =>
{
    bool res =Enigma.Receive.BLL.EnigmaPushBLL.SendMessage(message);

    return (res) ? new { Status = "OK" } : new { Status = "Erro" };

}).WithName("ReceiveNotificationMessage");

List<string> numbers = new List<string>();

app.MapPost("/notification", async (string number) =>
{
    bool res;

    var aux = numbers.Where(x => x == number).FirstOrDefault("false");
    if (aux == "false")
        numbers.Add(number);

    res = Enigma.Receive.BLL.EnigmaPushBLL.SendPhonesList(numbers);

    return (res) ? new { Status = "OK" } : new { Status = "Erro" };
}).WithName("ReceiveNotificationNumber");

app.Run();